# Multi-Currency Converter
> # Basic Info
> - University: City University of Hong Kong
> - Student: Lau Chi Chun Roy
> - Email: ROYLAU134@GMAIL.COM
You may refer to the [GitLab](https://gitlab.com/roylau134/CityU-EE4216-Individua_Assignment/) for more info.

# Software Require
 - Docker (v17.12) 
 - Docker Compose
  
# Installation Guide
Please refer to the [docker/readme.md](https://gitlab.com/roylau134/CityU-EE4216-Individua_Assignment/blob/master/docker/readme.md)

# Start the web
http://localhost/

# Technology used
 - Docker
 - Docker-Compose
 - Nginx (as static content web server)
 - HTML
 - CSS
 - JavaScript
 - jQuery (v3.3.1)
 - Bootstrap (v4.1.0)

# Features
## Basic Features
 - Base currencies and target currencies in list display (select up to 5 for each)
 - Edit the base amount
 - Show country flags (using [external resource](https://restcountries.eu/))
 - Refresh the target amounts automatically upon any change of inputs (the amount or the selection of the courrency)
 - Cache the exchange rates in the local storage of browser (stored as the name of currencyData)
 - Responsive layout (achived by using Bootstrap)

## Extra Feature
 - Showing related country info for each currency